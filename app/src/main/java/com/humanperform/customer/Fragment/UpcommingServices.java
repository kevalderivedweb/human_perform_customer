package com.humanperform.customer.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.humanperform.customer.Adapter.AdapterUpcomingBookService;
import com.humanperform.customer.R;

public class UpcommingServices extends Fragment {

    private RecyclerView recUpcomingBookService;
    private AdapterUpcomingBookService adapterUpcomingBookService;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.upcoming, container, false);

        recUpcomingBookService = view.findViewById(R.id.recUpcomingBookService);

        recUpcomingBookService.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterUpcomingBookService = new AdapterUpcomingBookService(getContext(), new AdapterUpcomingBookService.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recUpcomingBookService.setAdapter(adapterUpcomingBookService);



        return view;
    }


}