package com.humanperform.customer.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.humanperform.customer.Adapter.AdapterPastService;
import com.humanperform.customer.PastServiceInner;
import com.humanperform.customer.R;

public class PastServices extends Fragment {

    private RecyclerView recPastServices;
    private AdapterPastService adapterPastService;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.past_services, container, false);

        recPastServices = view.findViewById(R.id.recPastServices);
        recPastServices.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterPastService = new AdapterPastService(getContext(), new AdapterPastService.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getContext(), PastServiceInner.class));
            }
        });
        recPastServices.setAdapter(adapterPastService);


        return view;
    }


}