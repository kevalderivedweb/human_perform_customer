package com.humanperform.customer.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.humanperform.customer.ActiveServicesTimeSlot;
import com.humanperform.customer.Adapter.AdapterBlogs;
import com.humanperform.customer.Adapter.AdapterbookServices;
import com.humanperform.customer.BlogDetails;
import com.humanperform.customer.R;

public class Blog extends Fragment {


    private RecyclerView recBookedActive;
    private AdapterBlogs adapterCustomersDetails;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.blog, container, false);

        recBookedActive = view.findViewById(R.id.recBookedActive);
        recBookedActive.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterCustomersDetails = new AdapterBlogs(getActivity(), new AdapterBlogs.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getActivity(), BlogDetails.class));

            }
        });
        recBookedActive.setAdapter(adapterCustomersDetails);


        return view;
    }


}