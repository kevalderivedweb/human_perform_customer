package com.humanperform.customer.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.humanperform.customer.Adapter.AdapterService;
import com.humanperform.customer.Adapter.AdapterServiceWait;
import com.humanperform.customer.Adapter.ImageAdapter;
import com.humanperform.customer.Model.ServiceModel;
import com.humanperform.customer.ProductListActivity;
import com.humanperform.customer.R;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;

public class Store extends Fragment {


    private DotsIndicator indicator;
    private ArrayList<ServiceModel> mDatasetServices = new ArrayList<>();
    private ImageAdapter adapterView;
    private RecyclerView recService;
    private AdapterService adapterService;
    private AdapterServiceWait adapterServiceWait;
    private RecyclerView recService2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.store, container, false);

        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        indicator = (DotsIndicator) view.findViewById(R.id.worm_dots_indicator);
        adapterView = new ImageAdapter(getActivity(), mDatasetServices, new ImageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


            }
        });
        mViewPager.setAdapter(adapterView);
        indicator.setViewPager(mViewPager);

        recService = view.findViewById(R.id.recService);
        recService.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterService = new AdapterService(getContext(), new AdapterService.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
            }
        });
        recService.setAdapter(adapterService);



        recService2 = view.findViewById(R.id.recService2);
        recService2.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterServiceWait = new AdapterServiceWait(getContext(), new AdapterServiceWait.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


                openDialog();
            }
        });
        recService2.setAdapter(adapterServiceWait);



        view.findViewById(R.id.view_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),ProductListActivity.class));
            }
        });

        return view;
    }

    private void openDialog() {

        Dialog dialogForCity;
        dialogForCity = new Dialog(getContext());
        dialogForCity.setContentView(R.layout.custom_dialog_booking);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        dialogForCity.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogForCity.dismiss();

            }
        });


        dialogForCity.show();
    }


}