package com.humanperform.customer;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.humanperform.customer.Adapter.AdapterNotification;


public class NotificationActivity extends AppCompatActivity {

    private RecyclerView recListNotification;
    private AdapterNotification adapterNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);



        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(NotificationActivity.this,R.color.red));




        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        recListNotification = findViewById(R.id.recListNotification);
        recListNotification.setLayoutManager(new LinearLayoutManager(this));
        adapterNotification = new AdapterNotification(this, new AdapterNotification.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recListNotification.setAdapter(adapterNotification);



    }
}