package com.humanperform.customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class PastServiceInner extends AppCompatActivity {

    private AppCompatRatingBar ratingBarInnerHome;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_service_inner);
        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(PastServiceInner.this,R.color.red));

        ratingBarInnerHome = findViewById(R.id.ratingBarInnerHome);


        ratingBarInnerHome.setStepSize(0.1f);
        ratingBarInnerHome.setRating(Float.parseFloat("3.4"));
        ratingBarInnerHome.setIsIndicator(true);



        findViewById(R.id.giveRating).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });



    }

    private void openDialog() {

        Dialog dialogForCity;
        dialogForCity = new Dialog(PastServiceInner.this);
        dialogForCity.setContentView(R.layout.custom_dialog_rating);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        dialogForCity.show();
    }


}