package com.humanperform.customer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

public class Splash extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(Splash.this, IntroActivity.class));
                finish();
            }
        }, secondsDelayed * 2000);

       /* Random random = new Random(System.currentTimeMillis());
        int posOfImage = random.nextInt(yourListOfImages.length);
        ImageView imageView= (ImageView) findViewById(R.id.splashImage);
        imageView.setImageResource(yourListOfImages[posOfImage]);*/


    }


}