package com.humanperform.customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.humanperform.customer.Adapter.AdapterPaymentHistory;

public class PaymentHistory extends AppCompatActivity {

    private RecyclerView recPaymentHistory;
    private AdapterPaymentHistory adapterPaymentHistory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_history);


        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(PaymentHistory.this,R.color.red));

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        recPaymentHistory = findViewById(R.id.recPaymentHistory);
        recPaymentHistory.setLayoutManager(new LinearLayoutManager(this));
        adapterPaymentHistory = new AdapterPaymentHistory(this, new AdapterPaymentHistory.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recPaymentHistory.setAdapter(adapterPaymentHistory);




    }
}